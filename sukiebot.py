#!/usr/bin/python3
# https://discordapp.com/oauth2/authorize?client_id=508031670847275044&scope=bot&permissions=8

import discord
from discord.ext import commands
import logging

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

token = open("token.txt","r").read()
bot = commands.Bot(command_prefix='$')


@bot.command()
async def test(ctx, arg):
	await ctx.send(arg)
	
@bot.command()
async def ping(ctx):
    await ctx.send(str(bot.latency) + ' ms')

@bot.command(pass_context=True)
async def clear(ctx, amount: int):
    await ctx.channel.purge(limit=amount)


def community_report(guild):
	online = 0
	idle = 0
	offline = 0

	for m in guild.members:
		if str(m.status) == "online":
			online += 1
		if str(m.status) == "offline":
			offline += 1
		else:
			idle += 1

	return online, idle, offline

@bot.event  # event decorator/wrapper
async def on_ready():
	print(f"{bot.user} has logged in.")


@bot.event
async def on_message(message):
	print(f"{message.channel}: {message.author}: {message.author.name}: {message.content}")
	guild = message.channel.guild

	await bot.process_commands(message)

	if "duck_count" == message.content.lower():
		await message.channel.send(f"```py\n{guild.member_count}```")

	if "duck_report" == message.content.lower():
		online, idle, offline = community_report(guild)
		await message.channel.send(f"```Online: {online}.\nIdle/busy/dnd: {idle}.\nOffline: {offline}```")

	elif "oof" in message.content.lower():
		await message.channel.send("chill gin")
		

@bot.event
async def on_message_delete(message):
	#if message.author != bot.user:
	if "$clear" == message.content.lower():
		await message.channel.send(f"@{message.author} Why did you delete: ```{message.content}```")

		await bot.process_commands(message)


bot.run(token)
